package eezg.indra.servicio;

import eezg.indra.dao.IEstudianteDao;
import eezg.indra.domain.Estudiante;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//anotacion para que spring reconozca esta clase como clase de servicio
@Service
public class EstudianteServiceImpl implements EstudianteService{

    @Autowired
    private IEstudianteDao estudianteDao;
    
    @Override
    @Transactional(readOnly = true) //notacion para hacer commit o rollback en caso de error
    public List<Estudiante> listarEstudiantes() {
        return (List<Estudiante>) estudianteDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Estudiante estudiante) {
        estudianteDao.save(estudiante);
    }

    @Override
    @Transactional
    public void eliminar(Estudiante estudiante) {
        estudianteDao.delete(estudiante);
    }

    @Override
    @Transactional(readOnly = true)
    public Estudiante encontrarEstudiante(Estudiante estudiante) {
        return estudianteDao.findById(estudiante.getIdEstudiante()).orElse(null);
    }
    
}
