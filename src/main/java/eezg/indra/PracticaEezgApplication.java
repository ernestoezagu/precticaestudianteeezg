package eezg.indra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaEezgApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaEezgApplication.class, args);
	}

}
