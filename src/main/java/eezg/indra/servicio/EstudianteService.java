package eezg.indra.servicio;

import eezg.indra.domain.Estudiante;
import java.util.List;

public interface EstudianteService {
    
    public List<Estudiante> listarEstudiantes();
    
    public void guardar(Estudiante estudiante);
    
    public void eliminar(Estudiante estudiante);
    
    public Estudiante encontrarEstudiante(Estudiante estudiante);
    
}
