package eezg.indra.dao;

import eezg.indra.domain.Estudiante;
import org.springframework.data.repository.CrudRepository;

public interface IEstudianteDao extends CrudRepository<Estudiante, Long>{
    //springboot crea una implementacion de esta interface por default
}
