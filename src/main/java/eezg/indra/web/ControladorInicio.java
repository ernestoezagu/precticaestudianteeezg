package eezg.indra.web;

import eezg.indra.domain.Estudiante;
import eezg.indra.servicio.EstudianteService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j //anotacion para mandar mensajes a consola
public class ControladorInicio {
    
    @Autowired
    private EstudianteService estudianteService;
    
    @GetMapping("/")
    public String inicio(Model model){
        var estudiantes = estudianteService.listarEstudiantes();
        
        log.info("ejecutando el controlador Spring MVC");
        //para enviar informacion desde el controlador a la vista
        model.addAttribute("estudiantes", estudiantes);
        
        return "index";
    }
    
    @GetMapping("/agregar")
    public String agregar(Estudiante estudiante){
        return "modificar";
    }
    
    @PostMapping("/guardar")
    public String guardar(@Valid Estudiante estudiante, Errors errores){
        if(errores.hasErrors()){
            return "modificar";
        }
        estudianteService.guardar(estudiante);
        return "redirect:/";
    }
    
    @GetMapping("/editar/{idEstudiante}")
    public String editar(Estudiante estudiante, Model model){
        estudiante = estudianteService.encontrarEstudiante(estudiante);
        model.addAttribute("estudiante", estudiante);
        return "modificar";
    }
    
    @GetMapping("/eliminar")
    public String eliminar(Estudiante estudiante){
        estudianteService.eliminar(estudiante);
        return "redirect:/";
    }
}
